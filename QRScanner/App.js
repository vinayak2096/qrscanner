/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { SafeAreaView, Platform, StyleSheet, Text, View, Dimensions, TouchableOpacity, Alert, Linking, Clipboard } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { AdMobBanner } from 'react-native-admob';

// import {
//   AdMobBanner,
//   AdMobInterstitial,
//   PublisherBanner,
//   AdMobRewarded,
// } from 'react-native-admob'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height
var clipboardContent = ''

export default class App extends Component {

  state = {
    textToCopy: ''
  }

  componentDidMount() {
    this.scanner.reactivate()
  }

  onSuccess(e) {

    var pattern = (
      '^(http?https?:\/\/)?' +// protocol
      '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|)' +// domain name
      '((\d{1,3}\.){3}\d{1,3})' + // OR ip (v4) address
      '(\:\d+)?(\/[-a-z\d%_.~+]*)*' +  // port and path
      '(\[;&a-z\d%_.~+=-]*)' + // query string
      '(\#[-a-z\d_]*)?$', 'i'
    );

    var re = new RegExp(pattern);
    if (re.test(e.data)) {
      Alert.alert(
        '',
        e.data,
        [
          {
            text: '',
          },
          {
            text: 'Copy to clipboard', onPress: () => {
              Clipboard.setString(e.data)
              this.scanner.reactivate()
            }
          }
        ],
        { cancelable: false }
      )
    }
    else {
      Alert.alert(
        '',
        e.data,
        [
          {
            text: 'Go to Website', onPress: () => {
              Linking
                .openURL(e.data)
                .catch(err => alert('Something went wrong' + err))
              this.scanner.reactivate()
            }
          },
          {
            text: 'Copy to clipboard', onPress: () => {
              Clipboard.setString(e.data)
              this.scanner.reactivate()
            }
          }
        ],
        { cancelable: false }
      )
    }
  }

  getQRCode() {
    return (
      <QRCodeScanner
        ref={(node) => { this.scanner = node }}
        onRead={this.onSuccess.bind(this)}

        topContent={
          <Text style={styles.centerText}>
            <Text style={styles.textBold}>Scan QR Code</Text>
          </Text>
        }
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable}>
            {/* <Text style={styles.buttonText}>OK. Got it!</Text> */}
          </TouchableOpacity>
        }
      />
    )
  }
  render() {
    return (

      <QRCodeScanner
        ref={(node) => { this.scanner = node }}
        onRead={this.onSuccess.bind(this)}
        topViewStyle={{ backgroundColor: 'red' }}
        topContent={
          <Text style={styles.centerText}>
            <Text style={styles.textBold}>Scan QR Code</Text>
          </Text>
        }
        bottomViewStyle={{ marginTop: 10, backgroundColor: 'grey' }}
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable}>
            {/* <Text style={styles.buttonText}>OK. Got it!</Text> */}
            {/* <AdMobBanner
  adSize="Banner"
  adUnitID="ca-app-pub-3940256099942544/6300978111"
  testDevices={[AdMobBanner.simulatorId]}
  onAdFailedToLoad={error => console.error(error)}
/> */}

            <AdMobBanner
              adSize="banner"
              adUnitID="ca-app-pub-7479592788394026/5011181590"
              //  didFailToReceiveAdWithError={this.bannerError}
              onAdFailedToLoad={error => console.error(error)} />
          </TouchableOpacity>
        } />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
    // height: height * 0.9
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
    // height: height * 0.2
  }
});
